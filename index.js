console.log("ES6 UPDATES!");

// [SECTION] EXPONENT OPERATOR
	// Before ES6
		const firstNum = 8 ** 2;
		console.log(firstNum);

	// ES6
		/*
			syntax: Math.pow(base, exponent);
		*/
		const secondNum=Math.pow(8,2);
		console.log(secondNum);

// [SECTION] TEMPLATE LITERALS
	// allows us to write string without using concatenation operator (+);
	//greatly helps with code readability

		let name = "John";
			// Before ES6
			let message = "hello " + name + " ! Welcome to programming."
			console.log(message);

			// ES6
			// uses backticks(``)

			message = `Hello ${name} ! Welcome to programming!`
			console.log(message);

	// Template literals allows us to perform operations.
			const interestRate = 0.1;
			const principal = 1000;

			console.log(`The interest on you savings account is: ${interestRate*principal}.`)

//[SECTION] ARRAY DESTRUCTURING
	// Allows us to ubnpack elements in an array into distinct variables.
	// Allows us to name array elements with variables instead of using index number
		/*
			Syntax: 
			let/const [variableA, variableB, ..] = arrayName;

		*/
			const fullName = ["Juan", "Dela", "Cruz"];

				// Before ES6
					let firstName = fullName[0];
					let middleName = fullName[1];
					let lastName = fullName[2];

					console.log(`Hello ${firstName} ${middleName} ${lastName}`);

				// After ES6 udates
					const [fName, mName, lName] = fullName;

					console.log(fName);
					console.log(mName);
					console.log(lName);

		// Mini Activity 

			let array = [1,2,3,4,5];

			let [first, second, third, fourth, fifth] = array;

			console.log(first);
			console.log(second);
			console.log(third);
			console.log(fourth);
			console.log(fifth);

// [SECTION] OBJECT DESTRYUCTURING
	// allows us to un pack properties of object into distinct variable.

	/*
		syntax:
		let/const {propertyNameA, propertyNameB, ...} = objectName
	*/

			const person = {
				givenName : "Jane",
				maidenName:"Dela",
				familyName:"Cruz"
			}

			// Before ES6
			let gName = person.givenName;
			let midName = person.maidenName;
			let famName = person.familyName;
			console.log(gName);
			console.log(midName);
			console.log(famName);

			// After ES6 Update

			let {givenName, maidenName, familyName} = person


			console.log("Object destructuring after ES6 Updates");
			console.log(givenName);
			console.log(maidenName);
			console.log(familyName);

// [SECTION] ARROW FUNCTIONS
	// Compact alternative syntax to traditional functions
		const hello = () => { 
			console.log("Hello World!")
		}

		hello();

		/*// Function Expression
		const hello = function(){
			console.log("Hello World!");
		}

		// Function Declaration
			function hello(){
				console.log("hello World!");
			}*/


	//Implicit Return
	/*
		There are instances when you can omit return statement this work because even without using return keyword.
	*/ 
		const add = (x, y) => x+y;

		console.log("Implicit return");
		console.log(add(1,2));

		const subtract=(x, y) => x-y;
		console.log("Subtract");
		console.log(subtract(10,5));

// [SECTION] DEFAULT FUNCTION ARGUMENT VALUE
	// const greet = (firstName="firstName", lastName="lastName") => {
	// 	return `Good afternoon, ${firstName} ${lastName}!`;
	// }

	// console.log(greet("Danbell", "Villacampa"));

		function greet(firstname="firstName", lastName="lastName"){
			return `Good afternoon, ${firstName} ${lastName}!`;
		}
		console.log(greet());

// [SECTION] CLASS-BASED OBJECT BLUEPRINTS
	// allows us to create/instantiation of objects using classes blueprints
	// create class
		//constructor is a special method of a class of creating/initializing an object of the class

	/*
		Syntax: className{
			constructor(objectValueA, objectValueB, ...){
				this.objectPropertyA = objectValueA;
				this.objectPropertyB = objectValueB;
			}
		}
	*/

		class Car{
			constructor(brand, name, year){
				this.carBrand = brand;
				this.carName = name;
				this.carYear = year;
			}
		}

		let car = new Car("Toyota", "Hilux-pickup", 2015);
		console.log(car);